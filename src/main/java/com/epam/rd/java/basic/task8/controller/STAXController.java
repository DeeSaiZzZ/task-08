package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.classes.Car;
import com.epam.rd.java.basic.task8.classes.Creator;
import com.epam.rd.java.basic.task8.classes.Engine;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private final String xmlFileName;
    private final List<Car> cars = new ArrayList<>();
    private static final String SEARCHING_ELEMENT = "Car";
    private static final String INNER_COMPLEX_TYPE = "Engine";
    private Car car;
    private Engine engine;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Car> parse() throws FileNotFoundException, XMLStreamException {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
        while (reader.hasNext()) {
            XMLEvent xmlEvent = reader.nextEvent();
            if (xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();
                if (startElement.getName().getLocalPart().equals(SEARCHING_ELEMENT)) {
                    car = new Car();
                } else if (startElement.getName().getLocalPart().equals(INNER_COMPLEX_TYPE)) {
                    engine = new Engine();
                }
                if (engine != null) {
                    xmlEvent = reader.nextEvent();
                    Creator.setParam(startElement.getName().getLocalPart(), engine, xmlEvent.asCharacters().getData());
                } else if (car != null) {
                    xmlEvent = reader.nextEvent();
                    Creator.setParam(startElement.getName().getLocalPart(), car, xmlEvent.asCharacters().getData());
                }
            }
            if (xmlEvent.isEndElement()) {
                EndElement endElement = xmlEvent.asEndElement();
                if (endElement.getName().getLocalPart().equals(SEARCHING_ELEMENT)) {
                    car.setEngine(engine);
                    cars.add(car);
                    car = null;
                    engine = null;
                }
            }
        }
        return cars;
    }
}