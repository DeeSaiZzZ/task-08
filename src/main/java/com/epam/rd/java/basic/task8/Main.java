package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.classes.Car;
import com.epam.rd.java.basic.task8.classes.Cars;
import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.controller.STAXController;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////
        DOMController domController = new DOMController(xmlFileName);
        List<Car> domParseResult = domController.parse();

        domParseResult.sort(Comparator.comparing((Car::getGraduationYear)));

        String outputXmlFile = "output.dom.xml";
        writer(outputXmlFile, domParseResult);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////
        SAXController saxController = new SAXController(xmlFileName);
        List<Car> saxParseResult = saxController.parse();

        saxParseResult.sort((Comparator.comparing(Car::getEngineFuelType)));

        outputXmlFile = "output.sax.xml";
        writer(outputXmlFile, saxParseResult);

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////
        STAXController staxController = new STAXController(xmlFileName);
        List<Car> staxParseResult = staxController.parse();

        staxParseResult.sort(Comparator.comparing(Car::getTransmission));

        outputXmlFile = "output.stax.xml";
        writer(outputXmlFile, staxParseResult);
    }

    private static void writer(String fileName, List<Car> cars) {
        XmlMapper xmlMapper = new XmlMapper();
        Cars carWrapper = new Cars(cars);
        try (FileWriter fileWriter = new FileWriter(fileName)) {
            String xmlString = xmlMapper.writeValueAsString(carWrapper);
            fileWriter.write(xmlString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
