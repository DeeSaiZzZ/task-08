package com.epam.rd.java.basic.task8.classes;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Engine implements Serializable {

    @JsonProperty("Capacity")
    private double capacity;

    @JsonProperty("FuelType")
    private String fuelType;

    public Engine() {
        /*Constructor is empty because
        used to create an Engine shell*/
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    @Override
    public String toString() {
        return "Engine{" +
                "capacity=" + capacity +
                ", fuelType='" + fuelType + '\'' +
                '}';
    }
}
