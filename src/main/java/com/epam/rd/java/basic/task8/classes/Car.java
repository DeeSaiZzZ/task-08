package com.epam.rd.java.basic.task8.classes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;

@JacksonXmlRootElement(localName = "Car")
public class Car implements Serializable {

    @JsonProperty("Mark")
    private String mark;

    @JsonProperty("Model")
    private String model;

    @JsonProperty("GraduationYear")
    private int graduationYear;

    @JsonProperty("Color")
    private String color;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String lastRepair;

    @JsonProperty("Transmission")
    private String transmission;

    @JsonProperty("Engine")
    private Engine engine;

    public Car() {
        /*Constructor is empty because
        used to create a machine shell*/
    }

    @JsonIgnore
    public String getEngineFuelType() {
        return engine.getFuelType();
    }

    public String getTransmission() {
        return transmission;
    }

    public int getGraduationYear() {
        return graduationYear;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setGraduationYear(int graduationYear) {
        this.graduationYear = graduationYear;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setLastRepair(String lastRepair) {
        this.lastRepair = lastRepair;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String toString() {
        return "Car{" +
                "mark='" + mark + '\'' +
                ", model='" + model + '\'' +
                ", graduationYear=" + graduationYear +
                ", color='" + color + '\'' +
                ", lastRepair='" + lastRepair + '\'' +
                ", transmission='" + transmission + '\'' +
                ", engine=" + engine +
                '}';
    }
}
