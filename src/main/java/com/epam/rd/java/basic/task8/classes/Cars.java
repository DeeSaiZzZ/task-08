package com.epam.rd.java.basic.task8.classes;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.List;

public class Cars implements Serializable {
    @JacksonXmlProperty(isAttribute = true, localName = "xmlns")
    private static final String XMLNS = "www.legendaryCar.com";
    @JacksonXmlProperty(isAttribute = true, localName = "xmlns:xsi")
    private static final String XMLNS_XSI = "http://www.w3.org/2001/XMLSchema-instance";
    @JacksonXmlProperty(isAttribute = true, localName = "xsi:schemaLocation")
    private static final String SCHEMA_LOCATION = "www.legendaryCar.com input.xsd";

    @JacksonXmlProperty(localName = "Car")
    @JacksonXmlElementWrapper(useWrapping = false)
    private final List<Car> cars;

    public Cars(List<Car> cars) {
        this.cars = cars;
    }
}
