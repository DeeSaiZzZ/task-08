package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.classes.Car;
import com.epam.rd.java.basic.task8.classes.Creator;
import com.epam.rd.java.basic.task8.classes.Engine;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private final List<Car> carList = new ArrayList<>();
    private final String xmlFileName;
    private static final String SEARCHING_ELEMENT = "Car";
    private static final String INNER_COMPLEX_TYPE = "Engine";

    private String currentElement;

    private Car car;
    private Engine engine;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentElement = qName;
        if (currentElement.equals(SEARCHING_ELEMENT)) {
            car = new Car();
        } else if (currentElement.equals(INNER_COMPLEX_TYPE)) {
            engine = new Engine();
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String information = new String(ch, start, length);
        information = information.replace("\n", "").trim();
        if (!information.isEmpty()) {
            if (engine != null) {
                Creator.setParam(currentElement, engine, information);
            } else {
                Creator.setParam(currentElement, car, information);
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (qName.equals(SEARCHING_ELEMENT)) {
            car.setEngine(engine);
            carList.add(car);
            car = null;
            engine = null;
        }
    }

    public List<Car> parse() throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        parser.parse(xmlFileName, this);
        return carList;
    }

}