package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.classes.Car;
import com.epam.rd.java.basic.task8.classes.Creator;
import com.epam.rd.java.basic.task8.classes.Engine;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private final String xmlFileName;
    private final List<Car> cars = new ArrayList<>();
    private static final String SEARCHING_ELEMENT = "Car";
    private static final String INNER_COMPLEX_TYPE = "Engine";

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Car> parse() throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(xmlFileName);
        NodeList nodeList = document.getElementsByTagName(SEARCHING_ELEMENT);
        for (int i = 0; i < nodeList.getLength(); i++) {
            extractElement(nodeList.item(i).getChildNodes());
        }
        return cars;
    }

    private void extractElement(NodeList childNodes) {
        Car car = new Car();
        Engine engine = new Engine();

        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            if (node.getNodeType() != Node.TEXT_NODE) {
                if (node.getNodeName().equals(INNER_COMPLEX_TYPE)) {
                    extractComplexType(node.getChildNodes(), engine);
                } else {
                    Creator.setParam(node.getNodeName(), car, node.getTextContent());
                }
            }
        }
        car.setEngine(engine);
        cars.add(car);
    }

    private void extractComplexType(NodeList childNodes, Engine engine) {
        for (int j = 0; j < childNodes.getLength(); j++) {
            Node tepNode = childNodes.item(j);
            Creator.setParam(tepNode.getNodeName(), engine, tepNode.getTextContent());
        }
    }
}
