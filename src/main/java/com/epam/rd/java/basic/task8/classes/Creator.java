package com.epam.rd.java.basic.task8.classes;

public final class Creator {

    private Creator() {
    }

    public static void setParam(String elementName, Car car, String information) {
        switch (elementName) {
            case "Mark":
                car.setMark(information);
                break;
            case "Model":
                car.setModel(information);
                break;
            case "GraduationYear":
                car.setGraduationYear(Integer.parseInt(information));
                break;
            case "Color":
                car.setColor(information);
                break;
            case "LastRepair":
                car.setLastRepair(information);
                break;
            case "Transmission":
                car.setTransmission(information);
                break;
            default:
                break;
        }
    }

    public static void setParam(String elementName, Engine engine, String information) {
        if (elementName.equals("Capacity")) {
            engine.setCapacity(Double.parseDouble(information));
        } else if (elementName.equals("FuelType")) {
            engine.setFuelType(information);
        }
    }

}
